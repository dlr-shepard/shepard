package de.dlr.shepard.semantics;

public enum SemanticRepositoryType {
  SPARQL,
  JSKOS,
  SKOSMOS,
}

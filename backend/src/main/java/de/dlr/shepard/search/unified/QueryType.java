package de.dlr.shepard.search.unified;

public enum QueryType {
  StructuredData,
  Collection,
  DataObject,
  Reference,
}

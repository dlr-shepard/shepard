package de.dlr.shepard.search.container;

import de.dlr.shepard.search.ASearchParams;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ContainerSearchParams extends ASearchParams {

  @Valid
  @NotNull
  private ContainerQueryType queryType;

  public ContainerSearchParams(String query, ContainerQueryType queryType) {
    super(query);
    this.queryType = queryType;
  }
}

package de.dlr.shepard.search.container;

public enum ContainerQueryType {
  FILE,
  TIMESERIES,
  STRUCTUREDDATA,
}

package de.dlr.shepard.timeseries.model.enums;

public enum AggregateFunction {
  MEAN,
  MEDIAN,
  COUNT,
  SUM,
  MIN,
  MAX,
  LAST,
  FIRST,
  INTEGRAL,
  MODE,
  SPREAD,
  STDDEV,
}

package de.dlr.shepard.timeseries.model.enums;

public enum FillOption {
  LINEAR,
  NONE,
  NULL,
  PREVIOUS,
}

package de.dlr.shepard.timeseries.model.enums;

public enum ExperimentalDataPointValueType {
  Boolean,
  Integer,
  Double,
  String,
}

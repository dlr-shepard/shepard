package de.dlr.shepard.util;

public enum TraversalRules {
  children,
  parents,
  predecessors,
  successors,
}

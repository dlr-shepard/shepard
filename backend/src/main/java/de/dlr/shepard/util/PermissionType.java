package de.dlr.shepard.util;

public enum PermissionType {
  Public,
  PublicReadable,
  Private,
}

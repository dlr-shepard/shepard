package de.dlr.shepard.util;

public enum AccessType {
  Read,
  Write,
  Manage,
  None,
}

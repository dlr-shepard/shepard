package de.dlr.shepard.influxDB;

public enum SingleValuedUnaryFunction {
  MEAN,
  MEDIAN,
  COUNT,
  SUM,
  MIN,
  MAX,
  LAST,
  INTEGRAL,
  MODE,
  SPREAD,
  STDDEV,
  FIRST,
}

package de.dlr.shepard.influxDB;

public enum FillOption {
  LINEAR,
  NONE,
  NULL,
  PREVIOUS,
}

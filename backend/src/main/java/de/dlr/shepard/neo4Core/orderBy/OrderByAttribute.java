package de.dlr.shepard.neo4Core.orderBy;

public interface OrderByAttribute {
  boolean isString();
}

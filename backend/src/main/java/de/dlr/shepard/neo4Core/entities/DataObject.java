package de.dlr.shepard.neo4Core.entities;

import de.dlr.shepard.labJournal.entities.LabJournalEntry;
import de.dlr.shepard.util.Constants;
import de.dlr.shepard.util.HasId;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.Relationship.Direction;

@NodeEntity
@Data
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class DataObject extends AbstractDataObject {

  @Relationship(type = Constants.HAS_DATAOBJECT, direction = Direction.INCOMING)
  private Collection collection;

  @Relationship(type = Constants.HAS_REFERENCE)
  private List<BasicReference> references = new ArrayList<>();

  @Relationship(type = Constants.HAS_SUCCESSOR)
  private List<DataObject> successors = new ArrayList<>();

  @Relationship(type = Constants.HAS_SUCCESSOR, direction = Direction.INCOMING)
  private List<DataObject> predecessors = new ArrayList<>();

  @Relationship(type = Constants.HAS_CHILD)
  private List<DataObject> children = new ArrayList<>();

  @Relationship(type = Constants.HAS_CHILD, direction = Direction.INCOMING)
  private DataObject parent;

  @Relationship(type = Constants.POINTS_TO, direction = Direction.INCOMING)
  private List<DataObjectReference> incoming = new ArrayList<>();

  @Relationship(type = Constants.HAS_LABJOURNAL_ENTRY)
  private List<LabJournalEntry> labJournalEntries = new ArrayList<>();

  /**
   * For testing purposes only
   *
   * @param id identifies the entity
   */
  public DataObject(long id) {
    super(id);
  }

  public void addReference(BasicReference reference) {
    references.add(reference);
  }

  public void addSuccessor(DataObject successor) {
    successors.add(successor);
  }

  public void addPredecessor(DataObject predecessor) {
    predecessors.add(predecessor);
  }

  public void addChild(DataObject child) {
    children.add(child);
  }

  public void addIncoming(DataObjectReference dataObjectReference) {
    incoming.add(dataObjectReference);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + HasId.hashcodeHelper(collection);
    result = prime * result + HasId.hashcodeHelper(references);
    result = prime * result + HasId.hashcodeHelper(successors);
    result = prime * result + HasId.hashcodeHelper(predecessors);
    result = prime * result + HasId.hashcodeHelper(children);
    result = prime * result + HasId.hashcodeHelper(parent);
    result = prime * result + HasId.hashcodeHelper(incoming);
    result = prime * result + HasId.hashcodeHelper(labJournalEntries);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!super.equals(obj)) return false;
    if (!(obj instanceof DataObject)) return false;
    DataObject other = (DataObject) obj;
    return (
      HasId.equalsHelper(collection, other.collection) &&
      HasId.equalsHelper(references, other.references) &&
      HasId.equalsHelper(predecessors, other.predecessors) &&
      HasId.equalsHelper(successors, other.successors) &&
      HasId.equalsHelper(parent, other.parent) &&
      HasId.equalsHelper(children, other.children) &&
      HasId.equalsHelper(incoming, other.incoming) &&
      HasId.equalsHelper(labJournalEntries, other.labJournalEntries)
    );
  }
}

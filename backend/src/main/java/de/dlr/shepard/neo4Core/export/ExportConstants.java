package de.dlr.shepard.neo4Core.export;

public class ExportConstants {

  public static final String JSON_FILE_EXTENSION = ".json";
  public static final String CSV_FILE_EXTENSION = ".csv";
  public static final String ROCRATE_METADATA = "ro-crate-metadata.json";
  public static final String NAME_PROP = "name";
  public static final String DESCRIPTION_PROP = "description";
  public static final String CREATED_PROP = "dateCreated";
  public static final String UPDATED_PROP = "dateModified";
  public static final String TYPE_PROP = "additionalType";
}

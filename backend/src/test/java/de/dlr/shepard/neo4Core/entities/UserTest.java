package de.dlr.shepard.neo4Core.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import de.dlr.shepard.BaseTestCase;
import java.util.UUID;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

public class UserTest extends BaseTestCase {

  @Test
  public void equalsContract() {
    var a = UUID.randomUUID();
    var b = UUID.randomUUID();
    EqualsVerifier.simple()
      .forClass(User.class)
      .withPrefabValues(ApiKey.class, new ApiKey(a), new ApiKey(b))
      .withPrefabValues(Subscription.class, new Subscription(1L), new Subscription(2L))
      .verify();
  }

  @Test
  public void getUniqueIdTest() {
    var user = new User("bob");
    assertEquals("bob", user.getUniqueId());
  }

  @Test
  public void simpleConstructorTest() {
    var user = new User() {
      {
        setEmail("john.doe@example.com");
        setFirstName("John");
        setLastName("Doe");
        setUsername("bob");
      }
    };

    var actual = new User("bob", "John", "Doe", "john.doe@example.com");
    assertEquals(user, actual);
  }
}

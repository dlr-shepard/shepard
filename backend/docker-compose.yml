version: "3"

services:
  neo4j:
    image: neo4j:5.24
    environment:
      - NEO4J_AUTH=${NEO4J_AUTH-neo4j/shepardshepard}
    networks:
      - shepard
    ports:
      - 7474:7474
      - 7687:7687
    restart: unless-stopped

  mongodb:
    image: mongo:8.0
    networks:
      - shepard
    ports:
      - 27017:27017
    environment:
      - MONGO_INITDB_ROOT_USERNAME=${MONGO_INITDB_ROOT_USERNAME-mongo}
      - MONGO_INITDB_ROOT_PASSWORD=${MONGO_INITDB_ROOT_PASSWORD-shepard}
      - MONGO_USERNAME=${MONGO_USERNAME-username}
      - MONGO_PASSWORD=${MONGO_PASSWORD-password}
      - MONGO_INITDB_DATABASE=${MONGO_INITDB_DATABASE-database}
    restart: unless-stopped
    volumes:
      - ../infrastructure/docker-entrypoint-initdb.d/mongodb/00-create-mongo-user.js:/docker-entrypoint-initdb.d/00-create-mongo-user.js

  mongoexpress:
    image: mongo-express:latest
    networks:
      - shepard
    ports:
      - 8084:8081
    environment:
      - ME_CONFIG_MONGODB_SERVER=mongodb
      - ME_CONFIG_BASICAUTH_USERNAME=${MONGO_INITDB_ROOT_USERNAME-mongo}
      - ME_CONFIG_BASICAUTH_PASSWORD=${MONGO_INITDB_ROOT_PASSWORD-shepard}
      - ME_CONFIG_MONGODB_ENABLE_ADMIN=true
      - ME_CONFIG_MONGODB_ADMINUSERNAME=${MONGO_INITDB_ROOT_USERNAME-mongo}
      - ME_CONFIG_MONGODB_ADMINPASSWORD=${MONGO_INITDB_ROOT_PASSWORD-shepard}
    restart: unless-stopped
    depends_on:
      - mongodb

  timescaledb:
    image: timescale/timescaledb:latest-pg16
    networks:
      - shepard
    ports:
      - 5432:5432
    environment:
      POSTGRES_DB: ${POSTGRES_DB-postgres}
      POSTGRES_USER: ${POSTGRES_USER-postgres}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD-password}
      POSTGRES_SHEPARD_USER: ${POSTGRES_SHEPARD_USER-shepard}
      POSTGRES_SHEPARD_USER_PW: ${POSTGRES_SHEPARD_USER_PW-shepard_secret}
    restart: unless-stopped
    volumes:
      - ../infrastructure/docker-entrypoint-initdb.d/postgres/00-init-postgres-db.sh:/docker-entrypoint-initdb.d/00-init-postgres-db.sh

  influxdb:
    image: influxdb:1.8
    environment:
      - INFLUXDB_ADMIN_USER=${INFLUXDB_ADMIN_USER-influx}
      - INFLUXDB_ADMIN_PASSWORD=${INFLUXDB_ADMIN_PASSWORD-shepard}
    networks:
      - shepard
    ports:
      - 8086:8086
      - 8088:8088
    restart: unless-stopped

  chronograf:
    image: chronograf:latest
    environment:
      - INFLUXDB_URL=http://influxdb:8086
      - INFLUXDB_USERNAME=${INFLUXDB_ADMIN_USER-influx}
      - INFLUXDB_PASSWORD=${INFLUXDB_ADMIN_PASSWORD-shepard}
    networks:
      - shepard
    ports:
      - 8888:8888
    restart: unless-stopped
    depends_on:
      - influxdb

  frontend:
    image: registry.gitlab.com/dlr-shepard/shepard/frontend:dev
    environment:
      - VUE_APP_BACKEND=${VUE_APP_BACKEND-http://localhost:8080/shepard/api}
      - VUE_APP_OIDC_AUTHORITY=${VUE_APP_OIDC_AUTHORITY}
      - VUE_APP_CLIENT_ID=${VUE_APP_CLIENT_ID}
    ports:
      - 8081:80
    restart: unless-stopped
    profiles: [frontend]

  prometheus:
    image: prom/prometheus:latest
    command:
      - --web.enable-remote-write-receiver
      - --enable-feature=native-histograms
      - --config.file=/etc/prometheus/prometheus.yml
    networks:
      - shepard
    ports:
      - "9090:9090"
    volumes:
      - ../infrastructure/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
    restart: unless-stopped
    profiles: [monitoring]

  grafana:
    image: grafana/grafana:latest
    networks:
      - shepard
    ports:
      - "3300:3000"
    environment:
      - GF_AUTH_ANONYMOUS_ORG_ROLE=Admin
      - GF_AUTH_ANONYMOUS_ENABLED=true
      - GF_AUTH_BASIC_ENABLED=false
    volumes:
      - ./grafana:/etc/grafana/provisioning/
    restart: unless-stopped
    profiles: [monitoring]

networks:
  shepard:

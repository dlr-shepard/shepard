# shepard frontend

The shepard frontend is based on [Vue.js](https://vuejs.org/) and [OpenAPI Generator](https://openapi-generator.tech/).

## Useful Commands

- Install node modules: `npm install`
- Update packages: `npm update`
- Find outdated packages: `npm outdated`
- Run dev server: `npm run dev`
- Run linter: `npm run lint`
- Fix linting issues: `npm run fix`
- Compile for production: `npm run build`

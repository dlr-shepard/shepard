# clients/tests/python

This folder contains a python script to check if there are differences between the generated Python package in the remote registry and the Python package generated by the latest changes in this repositroy.
It is intended to be run in a pipeline job.
There, the generated client is already available as build artifact.

## Running it locally

> **NOTE:** You need docker and Python (ideally Python 3.12.5) installed locally to generate the Python client

To create the Python client call the following command.
**Make sure to call this command from the repository root level!**
It generates a Python client based on the `backend/target/openapi.json` file and stores the client package in `clients/python`:

- `docker run -it --rm -v ./clients/python:/clients/python -v ./backend/target/openapi:/backend/target/openapi openapitools/openapi-generator-cli:v7.7.0 docker-entrypoint.sh generate -i backend/target/openapi/openapi.json -g python -o clients/python -p=packageName=shepard_client`

Now change the directory into this folder:

- `cd clients/tests/python`

Then you have to rename and copy the generated source files to the `client/tests/python` folder with:

- `cp -r ../../python/shepard_client src/shepard_client_local`

Now you have to rename all occurences of the old local package name `shepard_client` to `shepard_client_local` with the following command:

- `grep -rl "from shepard_client." src/shepard_client_local/ | xargs sed -i 's/from shepard_client\./from shepard_client_local\./g'`

Then, create a python virtual env:

- `python -m venv env`

And activate it:

- `source env/bin/activate`

Install the needed requirements from this testing package and the client package with:

- `pip install -r requirements.txt`
- `pip install -r ../../python/requirements.txt`

Now you are ready to run the Python script with:

- `python src/main.py`

The script allows the following parameters:

```
usage: Python Client Diff Tool [-h] [-s] [-p] [-c]

options:
  -h, --help       show this help message and exit
  -p, --print-all  Print all differences, not just breaking ones.
  -c, --color      Use colored output for signature diffs
  -s, --signature  Compare function signatures and print colorful diff. Changing
                   function signatures are not checked for breaking changes. Only has
                   an effect when '-p' is set.
```

## Investigating found differences

When calling the script without any parameters, it compares both versions of the Python client package by importing all classes from packages and then:

1. compares class names, if there are classes in the 'old' package, which cannot be found in the newly generated package, this is considered a breaking change and is logged to the console.
2. compares the classes' attributes and methods by their names. Again, if there are attributes/ methods in the 'old' package's class, which are not in the 'new' package's class, this is considered a breaking change and the difference is logged to the console.
3. If a breaking change is noticed, the program exits with an exit code 1 and logs the error message to stderr.

When using the `-p` flag, then the other direction of changes is also logged. Meaning that classes, attributes, methods, which are in the 'new' client package, but cannot be found in the 'old' client package are logged to the console.
However, these differences are not considered breaking changes!

In addition, when using the `-s` flag, the program also compares the signatures of the classes' methods and prints their difference either line-by-line or with a colorful diff (`-c` flag).

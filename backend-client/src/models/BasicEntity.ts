/* tslint:disable */
/* eslint-disable */
/**
 * shepard backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-SNAPSHOT
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface BasicEntity
 */
export interface BasicEntity {
    /**
     * 
     * @type {number}
     * @memberof BasicEntity
     */
    readonly id: number;
    /**
     * 
     * @type {Date}
     * @memberof BasicEntity
     */
    readonly createdAt: Date;
    /**
     * 
     * @type {string}
     * @memberof BasicEntity
     */
    readonly createdBy: string;
    /**
     * 
     * @type {Date}
     * @memberof BasicEntity
     */
    readonly updatedAt: Date | null;
    /**
     * 
     * @type {string}
     * @memberof BasicEntity
     */
    readonly updatedBy: string | null;
    /**
     * 
     * @type {string}
     * @memberof BasicEntity
     */
    name: string;
}

/**
 * Check if a given object implements the BasicEntity interface.
 */
export function instanceOfBasicEntity(value: object): value is BasicEntity {
    if (!('id' in value) || value['id'] === undefined) return false;
    if (!('createdAt' in value) || value['createdAt'] === undefined) return false;
    if (!('createdBy' in value) || value['createdBy'] === undefined) return false;
    if (!('updatedAt' in value) || value['updatedAt'] === undefined) return false;
    if (!('updatedBy' in value) || value['updatedBy'] === undefined) return false;
    if (!('name' in value) || value['name'] === undefined) return false;
    return true;
}

export function BasicEntityFromJSON(json: any): BasicEntity {
    return BasicEntityFromJSONTyped(json, false);
}

export function BasicEntityFromJSONTyped(json: any, ignoreDiscriminator: boolean): BasicEntity {
    if (json == null) {
        return json;
    }
    return {
        
        'id': json['id'],
        'createdAt': (new Date(json['createdAt'])),
        'createdBy': json['createdBy'],
        'updatedAt': (json['updatedAt'] == null ? null : new Date(json['updatedAt'])),
        'updatedBy': json['updatedBy'],
        'name': json['name'],
    };
}

export function BasicEntityToJSON(value?: Omit<BasicEntity, 'id'|'createdAt'|'createdBy'|'updatedAt'|'updatedBy'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'name': value['name'],
    };
}


/* tslint:disable */
/* eslint-disable */
/**
 * shepard backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-SNAPSHOT
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import type { ExperimentalTimeseries } from './ExperimentalTimeseries';
import {
    ExperimentalTimeseriesFromJSON,
    ExperimentalTimeseriesFromJSONTyped,
    ExperimentalTimeseriesToJSON,
} from './ExperimentalTimeseries';
import type { ExperimentalTimeseriesDataPoint } from './ExperimentalTimeseriesDataPoint';
import {
    ExperimentalTimeseriesDataPointFromJSON,
    ExperimentalTimeseriesDataPointFromJSONTyped,
    ExperimentalTimeseriesDataPointToJSON,
} from './ExperimentalTimeseriesDataPoint';

/**
 * 
 * @export
 * @interface ExperimentalTimeseriesWithDataPoints
 */
export interface ExperimentalTimeseriesWithDataPoints {
    /**
     * 
     * @type {ExperimentalTimeseries}
     * @memberof ExperimentalTimeseriesWithDataPoints
     */
    timeseries: ExperimentalTimeseries;
    /**
     * 
     * @type {Array<ExperimentalTimeseriesDataPoint>}
     * @memberof ExperimentalTimeseriesWithDataPoints
     */
    points: Array<ExperimentalTimeseriesDataPoint>;
}

/**
 * Check if a given object implements the ExperimentalTimeseriesWithDataPoints interface.
 */
export function instanceOfExperimentalTimeseriesWithDataPoints(value: object): value is ExperimentalTimeseriesWithDataPoints {
    if (!('timeseries' in value) || value['timeseries'] === undefined) return false;
    if (!('points' in value) || value['points'] === undefined) return false;
    return true;
}

export function ExperimentalTimeseriesWithDataPointsFromJSON(json: any): ExperimentalTimeseriesWithDataPoints {
    return ExperimentalTimeseriesWithDataPointsFromJSONTyped(json, false);
}

export function ExperimentalTimeseriesWithDataPointsFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExperimentalTimeseriesWithDataPoints {
    if (json == null) {
        return json;
    }
    return {
        
        'timeseries': ExperimentalTimeseriesFromJSON(json['timeseries']),
        'points': ((json['points'] as Array<any>).map(ExperimentalTimeseriesDataPointFromJSON)),
    };
}

export function ExperimentalTimeseriesWithDataPointsToJSON(value?: ExperimentalTimeseriesWithDataPoints | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'timeseries': ExperimentalTimeseriesToJSON(value['timeseries']),
        'points': ((value['points'] as Array<any>).map(ExperimentalTimeseriesDataPointToJSON)),
    };
}


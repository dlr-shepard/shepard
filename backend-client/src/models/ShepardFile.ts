/* tslint:disable */
/* eslint-disable */
/**
 * shepard backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-SNAPSHOT
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ShepardFile
 */
export interface ShepardFile {
    /**
     * 
     * @type {string}
     * @memberof ShepardFile
     */
    readonly oid?: string;
    /**
     * 
     * @type {Date}
     * @memberof ShepardFile
     */
    readonly createdAt?: Date | null;
    /**
     * 
     * @type {string}
     * @memberof ShepardFile
     */
    readonly filename?: string;
    /**
     * 
     * @type {string}
     * @memberof ShepardFile
     */
    readonly md5?: string | null;
}

/**
 * Check if a given object implements the ShepardFile interface.
 */
export function instanceOfShepardFile(value: object): value is ShepardFile {
    return true;
}

export function ShepardFileFromJSON(json: any): ShepardFile {
    return ShepardFileFromJSONTyped(json, false);
}

export function ShepardFileFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShepardFile {
    if (json == null) {
        return json;
    }
    return {
        
        'oid': json['oid'] == null ? undefined : json['oid'],
        'createdAt': json['createdAt'] == null ? undefined : (new Date(json['createdAt'])),
        'filename': json['filename'] == null ? undefined : json['filename'],
        'md5': json['md5'] == null ? undefined : json['md5'],
    };
}

export function ShepardFileToJSON(value?: Omit<ShepardFile, 'oid'|'createdAt'|'filename'|'md5'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
    };
}


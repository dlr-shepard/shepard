/* tslint:disable */
/* eslint-disable */
/**
 * shepard backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-SNAPSHOT
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


/**
 * 
 * @export
 */
export const DataObjectAttributes = {
    CreatedAt: 'createdAt',
    UpdatedAt: 'updatedAt',
    Name: 'name'
} as const;
export type DataObjectAttributes = typeof DataObjectAttributes[keyof typeof DataObjectAttributes];


export function instanceOfDataObjectAttributes(value: any): boolean {
    for (const key in DataObjectAttributes) {
        if (Object.prototype.hasOwnProperty.call(DataObjectAttributes, key)) {
            if (DataObjectAttributes[key as keyof typeof DataObjectAttributes] === value) {
                return true;
            }
        }
    }
    return false;
}

export function DataObjectAttributesFromJSON(json: any): DataObjectAttributes {
    return DataObjectAttributesFromJSONTyped(json, false);
}

export function DataObjectAttributesFromJSONTyped(json: any, ignoreDiscriminator: boolean): DataObjectAttributes {
    return json as DataObjectAttributes;
}

export function DataObjectAttributesToJSON(value?: DataObjectAttributes | null): any {
    return value as any;
}


/* tslint:disable */
/* eslint-disable */
/**
 * shepard backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-SNAPSHOT
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface DataObject
 */
export interface DataObject {
    /**
     * 
     * @type {number}
     * @memberof DataObject
     */
    readonly id: number;
    /**
     * 
     * @type {Date}
     * @memberof DataObject
     */
    readonly createdAt: Date;
    /**
     * 
     * @type {string}
     * @memberof DataObject
     */
    readonly createdBy: string;
    /**
     * 
     * @type {Date}
     * @memberof DataObject
     */
    readonly updatedAt: Date | null;
    /**
     * 
     * @type {string}
     * @memberof DataObject
     */
    readonly updatedBy: string | null;
    /**
     * 
     * @type {string}
     * @memberof DataObject
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof DataObject
     */
    description?: string | null;
    /**
     * 
     * @type {{ [key: string]: string; }}
     * @memberof DataObject
     */
    attributes?: { [key: string]: string; };
    /**
     * 
     * @type {number}
     * @memberof DataObject
     */
    readonly collectionId: number;
    /**
     * 
     * @type {Array<number>}
     * @memberof DataObject
     */
    readonly referenceIds: Array<number>;
    /**
     * 
     * @type {Array<number>}
     * @memberof DataObject
     */
    readonly successorIds: Array<number>;
    /**
     * 
     * @type {Array<number>}
     * @memberof DataObject
     */
    predecessorIds?: Array<number>;
    /**
     * 
     * @type {Array<number>}
     * @memberof DataObject
     */
    readonly childrenIds: Array<number>;
    /**
     * 
     * @type {number}
     * @memberof DataObject
     */
    parentId: number | null;
    /**
     * 
     * @type {Array<number>}
     * @memberof DataObject
     */
    readonly incomingIds: Array<number>;
}

/**
 * Check if a given object implements the DataObject interface.
 */
export function instanceOfDataObject(value: object): value is DataObject {
    if (!('id' in value) || value['id'] === undefined) return false;
    if (!('createdAt' in value) || value['createdAt'] === undefined) return false;
    if (!('createdBy' in value) || value['createdBy'] === undefined) return false;
    if (!('updatedAt' in value) || value['updatedAt'] === undefined) return false;
    if (!('updatedBy' in value) || value['updatedBy'] === undefined) return false;
    if (!('name' in value) || value['name'] === undefined) return false;
    if (!('collectionId' in value) || value['collectionId'] === undefined) return false;
    if (!('referenceIds' in value) || value['referenceIds'] === undefined) return false;
    if (!('successorIds' in value) || value['successorIds'] === undefined) return false;
    if (!('childrenIds' in value) || value['childrenIds'] === undefined) return false;
    if (!('parentId' in value) || value['parentId'] === undefined) return false;
    if (!('incomingIds' in value) || value['incomingIds'] === undefined) return false;
    return true;
}

export function DataObjectFromJSON(json: any): DataObject {
    return DataObjectFromJSONTyped(json, false);
}

export function DataObjectFromJSONTyped(json: any, ignoreDiscriminator: boolean): DataObject {
    if (json == null) {
        return json;
    }
    return {
        
        'id': json['id'],
        'createdAt': (new Date(json['createdAt'])),
        'createdBy': json['createdBy'],
        'updatedAt': (json['updatedAt'] == null ? null : new Date(json['updatedAt'])),
        'updatedBy': json['updatedBy'],
        'name': json['name'],
        'description': json['description'] == null ? undefined : json['description'],
        'attributes': json['attributes'] == null ? undefined : json['attributes'],
        'collectionId': json['collectionId'],
        'referenceIds': json['referenceIds'],
        'successorIds': json['successorIds'],
        'predecessorIds': json['predecessorIds'] == null ? undefined : json['predecessorIds'],
        'childrenIds': json['childrenIds'],
        'parentId': json['parentId'],
        'incomingIds': json['incomingIds'],
    };
}

export function DataObjectToJSON(value?: Omit<DataObject, 'id'|'createdAt'|'createdBy'|'updatedAt'|'updatedBy'|'collectionId'|'referenceIds'|'successorIds'|'childrenIds'|'incomingIds'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'name': value['name'],
        'description': value['description'],
        'attributes': value['attributes'],
        'predecessorIds': value['predecessorIds'],
        'parentId': value['parentId'],
    };
}


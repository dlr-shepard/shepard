/* tslint:disable */
/* eslint-disable */
/**
 * shepard backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-SNAPSHOT
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Roles
 */
export interface Roles {
    /**
     * 
     * @type {boolean}
     * @memberof Roles
     */
    readonly owner: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof Roles
     */
    readonly manager: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof Roles
     */
    readonly writer: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof Roles
     */
    readonly reader: boolean;
}

/**
 * Check if a given object implements the Roles interface.
 */
export function instanceOfRoles(value: object): value is Roles {
    if (!('owner' in value) || value['owner'] === undefined) return false;
    if (!('manager' in value) || value['manager'] === undefined) return false;
    if (!('writer' in value) || value['writer'] === undefined) return false;
    if (!('reader' in value) || value['reader'] === undefined) return false;
    return true;
}

export function RolesFromJSON(json: any): Roles {
    return RolesFromJSONTyped(json, false);
}

export function RolesFromJSONTyped(json: any, ignoreDiscriminator: boolean): Roles {
    if (json == null) {
        return json;
    }
    return {
        
        'owner': json['owner'],
        'manager': json['manager'],
        'writer': json['writer'],
        'reader': json['reader'],
    };
}

export function RolesToJSON(value?: Omit<Roles, 'owner'|'manager'|'writer'|'reader'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
    };
}


/* tslint:disable */
/* eslint-disable */
/**
 * shepard backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-SNAPSHOT
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import type { SearchParams } from './SearchParams';
import {
    SearchParamsFromJSON,
    SearchParamsFromJSONTyped,
    SearchParamsToJSON,
} from './SearchParams';
import type { SearchScope } from './SearchScope';
import {
    SearchScopeFromJSON,
    SearchScopeFromJSONTyped,
    SearchScopeToJSON,
} from './SearchScope';

/**
 * 
 * @export
 * @interface SearchBody
 */
export interface SearchBody {
    /**
     * 
     * @type {SearchParams}
     * @memberof SearchBody
     */
    searchParams: SearchParams;
    /**
     * 
     * @type {Array<SearchScope>}
     * @memberof SearchBody
     */
    scopes: Array<SearchScope>;
}

/**
 * Check if a given object implements the SearchBody interface.
 */
export function instanceOfSearchBody(value: object): value is SearchBody {
    if (!('searchParams' in value) || value['searchParams'] === undefined) return false;
    if (!('scopes' in value) || value['scopes'] === undefined) return false;
    return true;
}

export function SearchBodyFromJSON(json: any): SearchBody {
    return SearchBodyFromJSONTyped(json, false);
}

export function SearchBodyFromJSONTyped(json: any, ignoreDiscriminator: boolean): SearchBody {
    if (json == null) {
        return json;
    }
    return {
        
        'searchParams': SearchParamsFromJSON(json['searchParams']),
        'scopes': ((json['scopes'] as Array<any>).map(SearchScopeFromJSON)),
    };
}

export function SearchBodyToJSON(value?: SearchBody | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'searchParams': SearchParamsToJSON(value['searchParams']),
        'scopes': ((value['scopes'] as Array<any>).map(SearchScopeToJSON)),
    };
}


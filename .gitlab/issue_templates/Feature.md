# Feature Request

<!--
Thanks for contributing to our project!
Before creating a feature request, please read this first:

- Use the template below
- Check if there are related issues
- Determine where the request should be reported:
  https://gitlab.com/dlr-shepard/shepard/-/blob/main/CONTRIBUTING.md#how-can-i-contribute
- Use a clear and descriptive title for the issue to identify the suggestion
- For code snippets and logs please use Markdown code blocks:
  https://docs.gitlab.com/ee/user/markdown.html

-->

## :goal_net: Goal

As a `role` I want `feature` so that `reason`.

## :sports_medal: Acceptance Criteria

1.
2.
3.

## :star: Special Testing hints

## :bomb: Out of Scope

## :bulb: Hints

## :thinking_face: Risks & Open Questions

/label ~"issue::feature"

# Bug Report

<!--
Thanks for contributing to our project!
Before creating a bug report, please read this first:

- Use the template below
- Check if there are related issues
- Determine where the issue should be reported:
  https://gitlab.com/dlr-shepard/shepard/-/blob/main/CONTRIBUTING.md#how-can-i-contribute
- Use a clear and descriptive title for the issue to identify the bug report
- If you encounter a security issue, please mark this bug report as confidential
- For code snippets and logs please use Markdown code blocks:
  https://docs.gitlab.com/ee/user/markdown.html

-->

## :bug: Summary

## :clipboard: Steps to reproduce

1.
2.
3.

## :package: Environment

<!-- You can find out the exact version on the "About" page -->

- shepard Version:
- Webbrowser and version:
- shepard client and version:
- Operating system:

## :bomb: What is the current bug behavior?

## :goal_net: What is the expected correct behavior?

## :notebook: Relevant logs and/or screenshots

<!-- Paste all relevant logs from your web browser's console/inspector, as well as screenshots and animated GIFs here. -->

```txt
Paste logs here
```

## :bulb: Hints

## :thinking_face: Risks & Open Questions

/label ~"issue::bug"

# Refactoring Task

<!--
Thanks for contributing to our project!
Before creating a refactoring task, please read this first:

- Use the template below
- Check if there are related issues
- Determine where the issue should be reported:
  https://gitlab.com/dlr-shepard/shepard/-/blob/main/CONTRIBUTING.md#how-can-i-contribute
- Use a clear and descriptive title for the issue to identify the issue
- For code snippets and logs please use Markdown code blocks:
  https://docs.gitlab.com/ee/user/markdown.html

-->

## :mount_fuji: Summary

## :bomb: Why is the current state bad?

## :goal_net: How could that be improved?

## :bulb: Hints

## :thinking_face: Risks & Open Questions

/label ~"issue::refactoring"
